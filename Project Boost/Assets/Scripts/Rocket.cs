﻿/*
 *   Copyright (c) 2020 NotSlimBoy
 *   All rights reserved.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    Rigidbody rigidbody;
    AudioSource audioSource;
    // * Rocket Value
    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float RotateSpeed = 100f;
    [SerializeField] float MainSpeed = 50f;
    [SerializeField] float LevelLoadDelay = 2f;
    // * Sound Clip
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip success;
    [SerializeField] AudioClip death;
    // * Particle System
    [SerializeField] ParticleSystem RocketJetParticles;
    [SerializeField] ParticleSystem SuccessLandingParticles;
    [SerializeField] ParticleSystem DeathParticles;
    // * Game State 
    enum State { Alive, Dying, Transcending }
    State state = State.Alive;

    // *  Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // *  Update is called once per frame
    void Update()
    {
        if (state == State.Alive)
        {
            ProcessInput();
        }
    }

    // * Process Input User
    private void ProcessInput()
    {
        ThrusterBoost();
        ThrusterRotate();
    }

    // * Collision With Tag
    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.Alive) { return; } // * Guard Condition

        switch (collision.gameObject.tag)
        {
            case "Safe":
                print("Safe");
                break;
            case "Landing":
                StartSuccesSequence();
                break;
            case "Wall":
                StartDeathSequence();
                break;
            default:
                break;
        }
    }

    //  * Succes Sequence
    private void StartSuccesSequence()
    {
        state = State.Transcending;
        audioSource.Stop();
        audioSource.PlayOneShot(success);
        SuccessLandingParticles.Play();
        Invoke("LoadNextScene", LevelLoadDelay);
    }

    //  * Death Sequeence
    private void StartDeathSequence()
    {
        state = State.Dying;
        audioSource.Stop();
        RocketJetParticles.Stop();
        audioSource.PlayOneShot(death);
        DeathParticles.Play();
        Invoke("BackToFirstLevel", LevelLoadDelay);
    }

    // * Scene Management
    private void BackToFirstLevel()
    {
        SceneManager.LoadScene(0);
    }

    // * Scene Management
    private void LoadNextScene()
    {
        SceneManager.LoadScene(1);
    }

    // * Rocket Booster
    private void ThrusterBoost()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();
        }
        else
        {
            StopApllyingThrust();
        }
    }

    private void StopApllyingThrust()
    {
        audioSource.Stop();
        RocketJetParticles.Stop();
    }

    // * Thrusting
    private void ApplyThrust()
    {
        rigidbody.AddRelativeForce(Vector3.up * MainSpeed);
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }

        RocketJetParticles.Play();
    }

    // * Rocket Manuveuring
    private void ThrusterRotate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RotateManually(rcsThrust * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RotateManually(-rcsThrust * Time.deltaTime);
        }
    }

    // * Rotate Manually Handle
    private void RotateManually(float rotationThisFrame)
    {
        rigidbody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame);
        rigidbody.freezeRotation = false;
    }

}
