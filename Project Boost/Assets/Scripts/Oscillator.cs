﻿/*
 *   Copyright (c) 2020 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// * Disallow Multiple Object or Script
[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector = new Vector3();
    [SerializeField] float Period = 2f;
    float movementFactor;
    Vector3 startingPos;
    private void Start()
    {
        startingPos = transform.position;
    }
    private void Update()
    {
        OscillatorHandler();
    }

    private void OscillatorHandler()
    {
        if (Period <= Mathf.Epsilon) { return; } // * Guard Condition to NAN, cause NAN is equal to 0 = 0 
        float cycles = Time.time / Period; // *  grows continually from 0
        const float tau = Mathf.PI * 2f; // *  about 6.28
        float rawSinWave = Mathf.Sin(cycles * tau); // *  goes from -1 to +1
        movementFactor = rawSinWave / 2f + 0.5f;
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
    }
}
